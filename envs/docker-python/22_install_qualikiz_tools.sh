#!/bin/sh

# Re-use existing install if it exists. Otherwise, reclone
QLK_PYTHONTOOLS_DIR=../QuaLiKiz-pythontools-qlknn
if [[ -d "$QLK_PYTHONTOOLS_DIR" ]]; then
    pip install --upgrade $QLK_PYTHONTOOLS_DIR
    echo "Using existing git repository located at $QLK_PYTHONTOOLS_DIR"
else
    (git clone https://gitlab.com/qualikiz-group/QuaLiKiz-pythontools.git $QLK_PYTHONTOOLS_DIR
    cd $QLK_PYTHONTOOLS_DIR
    pip install --upgrade .)
fi
