# makefile for QLKNN-develop library
PYTHON?=python
PIP?=pip
POETRY?=poetry
ECHO?=$(shell which echo) -e
PRINTF?=$(shell which printf)

# Variables required for release and bugfix options
PROJECT=QLKNN-develop
PROJECT_ID=10189900
PACKAGE=qlknn

# Other variables
SUBDIRS:=

#####################################################

.PHONY: help clean sdist bdist wheel install_package_jintrac upload_package_to_gitlab install_package_from_gitlab docs realclean

help:
	@$(ECHO) "Recipes:"
	@$(ECHO) "  docs                        - build documentation for package"
	@$(ECHO) "  clean                       - remove all build, test, doc, and Python artifacts"
	@$(ECHO) "  realclean                   - clean and remove build distributions "
	@$(ECHO) ""
	@$(ECHO) "Environment variables:"
	@$(ECHO) "  PYTHON                      - Python binary to use for commands [default: "$(shell grep -e PYTHON?\= Makefile | cut -d\= -f2)"]"
	@$(ECHO) "  PIP                         - Pip binary to use for commands [default: "$(shell grep -e PIP?\= Makefile | cut -d\= -f2)"]"
	@$(ECHO) "  POETRY                      - Poetry binary to use for commands [default: "$(shell grep -e POETRY?\= Makefile | cut -d\= -f2)"]"
	@$(ECHO) "  JINTRAC_PYTHON_INSTALL_DIR  - JINTRAC install directory [default: "$(shell grep -e JINTRAC_PYTHON_INSTALL_DIR?\= Makefile | cut -d\= -f2)"]"
	@$(ECHO) "  PYTHONTOOLS_EXTRAS          - Extras to install [default: "$(shell grep -e PYTHONTOOLS_EXTRAS?\= Makefile | cut -d\= -f2)"]"

docs:
	$(MAKE) -C docs html

clean:
	@echo 'Cleaning $(PROJECT)...'
	poetry cache clear $(PACKAGE) --all
	$(MAKE) -C docs $@

realclean: clean
	@echo 'Real cleaning $(PROJECT)...'
	rm -f dist/*
	$(MAKE) -C docs $@
