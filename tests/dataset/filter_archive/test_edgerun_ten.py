"""
This file is part of QLKNN-develop.
You should have received QLKNN-develop LICENSE file with this project.
"""
from pathlib import Path
import re

import pexpect
from pexpect import spawn as Spawn
from IPython import embed  # pylint: disable=unused-import # noqa: F401
from qlknn.dataset.filter_archive.edgerun_ten import *

def test_run_filter(tmpdir, running_command):
    dump_package_versions() # test if import worked
    root = Path(__file__).parent.parent.parent.parent
    script_path = root / "qlknn/dataset/filter_archive/edgerun_ten.py"
    data_path = root / "testdata/gen5_test_files/cut_folded_netcdf.nc.1"

    # Test existence of files and prepare runcommand
    assert script_path.exists(), "To be tested script file does not exist"
    assert data_path.exists(), "To be tested netCDF4 file does not exist"
    filter_num = 1000
    cmd = f"python {script_path} {data_path} cut_ {filter_num}"
    running_command.shell_cmd = cmd
    with tmpdir.as_cwd():
        child: Spawn = running_command.spawn()


        # Wait for child to start, as dump_package_versions should always be called
        index = child.expect([pexpect.EOF, pexpect.TIMEOUT, "numpy is version \d+.\d+.\d+"])
        assert index == 2, "dump_package_versions is never called in script"

        print("Check if filter script done")
        child.expect("Filter script done", timeout=running_command.timeout)

        logname = "python-\d+.log"
        logfile = None
        for file in tmpdir.listdir():
            if re.match(f".*{logname}", str(file)):
                if logfile is None:
                    logfile = file
                else:
                    assert False, "Multiple logfiles found"

        assert logfile.exists(), "No logfile generated"

        genfiles = [
            "gam_cache.nc",
            "prepared_netcdf.nc",
            f"cut_gen5_9D_pedformreg10_filter{filter_num}.h5.1",
            f"sane_cut_gen5_9D_pedformreg10_filter{filter_num}.h5.1",
            f"test_cut_gen5_9D_pedformreg10_filter{filter_num}.h5.1",
            f"training_cut_gen5_9D_pedformreg10_filter{filter_num}.h5.1",
            f"unstable_test_cut_gen5_9D_pedformreg10_filter{filter_num}.h5.1",
            f"unstable_training_cut_gen5_9D_pedformreg10_filter{filter_num}.h5.1",
        ]

        for genfile in genfiles:
            assert Path(genfile).exists()
